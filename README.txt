
Description
------------

SMSEmail module allows to send SMS messages via e-mail gateway.
There is a singe API function smsemail_send($phone, $message, $notify = FALSE)
which can be used by other modules anywhere on your site.
  Parameters:
    1. $phone   - a full international phone number of the recipient.
    2. $message - an SMS message text.
    3. $notify  - if set to TRUE, drupal_set_message() will be called in case of an error.
  Return value:
    A resulting recipient email address or FALSE if message hasn't been sent.

Usually, sending SMS messages through email gateways are not guaranteed or sending
delay may be valuable or even huge. An advantage is that this is free and you
don't need to pay for an outgoing SMS. So, use this module only when sending SMS
messages is a secondary feature of your site and nobody will suffer if he
receives a message with a delay or doesn't receive it at all (i.e. send a welcome
SMS upon registering an account). Otherwize, you'd better buy an account in a
special company (i.e. clickatel, x-on) and use smsgateway.module instead.

To successfully send a message to a desired number you need the following:

1. Find either mobile company supports sending SMS mesages through email gateways.
2. Find a gateway domain which corresponds to a prefix of the desired number.
3. Find which number format (mode) should be used in email address (it can
   be either full international format including '+' sign, or full number
   without '+' sign, or just a local number within the operator without prefix).
4. Install this module and configure gateways properly.

IMPORTANT: In order to receive an SMS sent via email gateway a user must enable
this feature in his operator. Many operators disable this feature by default.
Don't forget to warn users somewhere on your site to check if they are allowed
to receive SMS message sent through email.

Installation & Configuring
--------------------------

To install this module, you need the following:

1. Copy smsemail module in your Drupal's modules directory.

2. Go to 'admin/build/modules' page and turn smsemail.module on.

3. Configure smsemail.module on 'admin/settings/smsemail' page. Add as many
   gateways as you need (every gateway corresponds to a certain number prefix).
   For instance, to send 

4. There is a test page where you can try to send an SMS to a specified number.
